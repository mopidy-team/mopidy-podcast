Mopidy>=3.0.0
Pykka>=2.0.1
cachetools>=2.0
setuptools
uritools>=1.0

[dev]
sphinx
black
check-manifest
flake8
flake8-black
flake8-bugbear
flake8-import-order
isort[pyproject]
twine
wheel
pytest
pytest-cov

[docs]
sphinx

[lint]
black
check-manifest
flake8
flake8-black
flake8-bugbear
flake8-import-order
isort[pyproject]

[release]
twine
wheel

[test]
pytest
pytest-cov
